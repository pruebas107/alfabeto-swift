//
//  AlfabetoTableViewController.swift
//  App2_TableViewController_
//
//  Created by Felipe Hernandez on 14/10/22.
//

import UIKit

class AlfabetoTableViewController: UITableViewController {

                          //Array(alfabeto.lugares.keys).sorted($0 < $1)
    let alfabetoArreglo = Array(alfabeto.lugares.keys).sorted {
        
        let str1 =  $0 as NSString
        let str2 = $1 as NSString
        
        let locale  =  NSLocale(localeIdentifier: "es_MX")
        return  str1.compare(str2 as String,
                             options: .caseInsensitive,
                             range: NSMakeRange(0, str1.length),
                             locale: locale) == ComparisonResult.orderedAscending
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        print(alfabetoArreglo)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(segueNuevoAlfabeto))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tableView.reloadData()
    }
    
    @objc func segueNuevoAlfabeto(){
        performSegue(withIdentifier: "segueNuevoAlfabeto", sender: self)
    }
    

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return alfabeto.lugares.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        let letra =  alfabetoArreglo[section]
        
        switch section {
        case 0...alfabetoArreglo.count:
            return alfabeto.lugares[letra]!.count
        default:
            return 1
        }
    }

  
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomCell", for: indexPath)

        let letra =  alfabetoArreglo[indexPath.section]

        switch indexPath.section {
        case 0...alfabetoArreglo.count:
            cell.textLabel?.text = alfabeto.lugares[letra]?[indexPath.row]
        default: break
        }
        
        return cell
    }
    

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        let letra =  alfabetoArreglo[section]
        
        switch section {
        case 0...alfabetoArreglo.count: return letra
        default: return letra
        }
    }
    
    override func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return alfabetoArreglo
    }

}
