//
//  AltaViewController.swift
//  App2_TableViewController_
//
//  Created by Felipe Hernandez on 14/10/22.
//

import UIKit

class AltaViewController: UIViewController {

    
    @IBOutlet var txtDato: UITextView!
    
    
    @IBAction func onAgregar(_ sender: Any) {
        
        if !txtDato.text.isEmpty {
            
            modelo.datos.append(txtDato.text)
            
        }
        
        navigationController?.popViewController(animated: true)
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtDato.becomeFirstResponder()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
