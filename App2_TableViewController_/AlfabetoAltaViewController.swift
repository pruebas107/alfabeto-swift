//
//  AlfabetoAltaViewController.swift
//  App2_TableViewController_
//
//  Created by Giggs Gam on 18/10/22.
//

import UIKit

class AlfabetoAltaViewController: UIViewController {

    
    @IBOutlet var txtAlfabeto: UITextView!
    
    
    @IBAction func onAgregar(_ sender: Any) {
        if !txtAlfabeto.text.isEmpty {
            
            let key = txtAlfabeto.text.prefix(1).uppercased()
            
            //alfabeto.lugares[(String(txtAlfabeto.text.prefix(1))),default: [txtAlfabeto.text]]
            //alfabeto.lugares[String(key)]?.insert(contentsOf: String(txtAlfabeto.text), at: Int(alfabeto.lugares.endIndex))
            
            alfabeto.lugares[String(key)]?.append(txtAlfabeto.text)
            
            print(txtAlfabeto.text!)
            
            print(key)
            
        }
        
        navigationController?
            .popViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtAlfabeto.becomeFirstResponder()
        
    }
}
