//
//  TableViewRefreshindicatorViewController.swift
//  App2_TableViewController_
//
//  Created by MTWDM_2022 on 07/10/22.
//

import UIKit

class TableViewRefreshindicatorViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITableViewDragDelegate  {


    
    @IBOutlet var TableViewCustom: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.leftBarButtonItem = self.editButtonItem;
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(segueNuevoElemento));
        
        TableViewCustom.refreshControl = pullToRefreshControl;
        TableViewCustom.dataSource = self
        TableViewCustom.delegate = self
         TableViewCustom.dragDelegate = self
        TableViewCustom.dragInteractionEnabled = true
        
        pullToRefreshControl.addTarget(self, action:  #selector(refreshTable), for: UIControl.Event.valueChanged);

        // Do any additional setup after loading the view.
    }
    
    
    
    let pullToRefreshControl = UIRefreshControl();

    //MARK: NUEVO MÉTODO
    override func viewWillAppear(_ animated: Bool) {
        TableViewCustom.reloadData()
    }
    
    //MARK: NUEVO ELEMENTO 
    @objc func segueNuevoElemento(){
        performSegue(withIdentifier: "segueNuevoElemento", sender: self)
    }
    
    @objc func refreshTable() {
        modelo.datosNube = modelo.datosNube.reversed();
        modelo.datos = modelo.datosNube;
        self.TableViewCustom.reloadData();
        pullToRefreshControl.endRefreshing();
    }
    
    
    
    func tableView(_ tableView: UITableView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
        
        let dragItem = UIDragItem(itemProvider: NSItemProvider())
        
        dragItem.localObject = modelo.datos[indexPath.row];
        
        return [ dragItem ]
    }
    
     func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        // Update the model
        let mover = modelo.datos.remove(at: sourceIndexPath.row)
        modelo.datos.insert(mover, at: destinationIndexPath.row)
    }
    
 
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return modelo.datos.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell =  tableView.dequeueReusableCell(withIdentifier:  "CustomCell", for: indexPath);
        cell.textLabel?.text = modelo.datos[indexPath.row];
        
        return cell;
    }
    
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true;
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            modelo.datos.remove(at: indexPath.row);
            TableViewCustom.deleteRows(at: [indexPath], with: .left);
        } else if editingStyle == .insert {
        
        }
    }
    
    
     func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true;
    }
    
    override func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        TableViewCustom.setEditing(editing, animated: true)
        TableViewCustom.reloadData()
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
